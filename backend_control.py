import picar_4wd as fc
import time
import socket
from gpiozero import CPUTemperature

HOST = "192.168.50.110" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)
distance_traveled = 0


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((HOST, PORT))
    s.listen()

    try:
        while 1:	    
            client, clientInfo = s.accept()
            print("server recv from: ", clientInfo)
            data = client.recv(1024)      # receive 1024 Bytes of message in binary formaty
            if data != b"":

                print(data)

                arr = data.decode("utf-8").split('|')

                print (arr)
                if (arr[0] == "Going Forward"):
                    fc.forward(100)
                    time.sleep(0.05)
                    fc.stop()
                    distance_traveled += 1
                if (arr[0] == "Turning Left"):
                    fc.turn_left(100)
                    time.sleep(0.05)
                    fc.stop()
                if (arr[0] == "Going Backwards"):
                    fc.backward(100)
                    time.sleep(0.05)
                    fc.stop()
                    distance_traveled -= 1
                if (arr[0] == "Turning Right"):
                    fc.turn_right(100)
                    time.sleep(0.05)
                    fc.stop()

               
                
                send_data = str(arr[0]) + "|" + str(arr[1]) + "|" + str(distance_traveled) + "|" + str(CPUTemperature().temperature) + "|" + str(fc.power_read())

                client.sendall(send_data.encode("utf-8")) # Echo back to client
                
                if (arr[1] == "stop"):
                    break
    except: 
        print("Closing socket")
        client.close()
        s.close()    
